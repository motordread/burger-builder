import React from 'react';

import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';
import classes from './Summary.css';

const summary = (props) => {
    return (
        <div className={classes.Summary}>
            <h1>Your burger checkout</h1>
            <div style={{width: '100%', margin: 'auto'}}>
                <Burger ingredients={props.ingredients} />
            </div>
            <Button clicked={props.checkoutCancelled} btnType="Danger">Cancel</Button>
            <Button clicked={props.checkoutContinued} btnType="Success">Success</Button>
        </div>
    );
};

export default summary;