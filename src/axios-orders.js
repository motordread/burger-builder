import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://burger-1a4d3.firebaseio.com'
});

export default instance;